package com.rvslabs.openfire.plugin.geochat.component;

import com.rvslabs.openfire.plugin.geochat.dao.UserDAO;
import com.rvslabs.openfire.plugin.geochat.entity.UserModel;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;
import com.rvslabs.openfire.plugin.geochat.listeners.UserLocationDispatcher;
import com.rvslabs.openfire.plugin.geochat.listeners.UserLocationListener;
import org.xmpp.component.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rvslabs on 21/07/15.
 */
public class UserChatManager extends ChatManager implements UserLocationListener {


    public static final String NEARBY_ROOM = "nearby";
    public static final String REGION_ROOM = "region";

    protected UserChatManager(Component component) {
        super(component);
        UserLocationDispatcher.addListener(this);
    }

    @Override
    protected List<String> getUsers(String user, String room) throws ServiceException {
        switch (room) {
            case NEARBY_ROOM:
                return UserDAO.getNearbyUsersFromUser(user);
            case REGION_ROOM:
                return UserDAO.getRegionUsersFromUser(user);
            default:
                return new ArrayList<>();
        }
    }

    @Override
    protected List<String> getOnlineUsers(String user, String room) throws ServiceException {
        switch (room) {
            case NEARBY_ROOM:
                return UserDAO.getNearbyOnlineUsersFromUser(user);
            case REGION_ROOM:
                return UserDAO.getRegionOnlineUsersFromUser(user);
            default:
                return new ArrayList<>();
        }
    }

    @Override
    protected void connectUserToRoom(String user, String room) throws ServiceException {
        switch (room) {
            case NEARBY_ROOM:
                UserDAO.connectUserToNearbyChat(user, true);
                break;
            case REGION_ROOM:
                UserDAO.connectToRegionChat(user, true);
                break;
            default:
                break;
        }
    }

    @Override
    protected void disconnectUserToRoom(String user, String room) throws ServiceException {
        switch (room) {
            case NEARBY_ROOM:
                UserDAO.connectUserToNearbyChat(user, false);
                break;
            case REGION_ROOM:
                UserDAO.connectToRegionChat(user, false);
                break;
            default:
                break;
        }
    }

    @Override
    public void userChangeLocation(UserModel user, String fromLon, String fromLat, String toLon, String toLat) {
        if (user.isNearby()) {
            // Pegar todos os usuarios da antiga
            List<String> oldUsersIds = null;
            try {
                oldUsersIds = UserDAO.getNearbyUsersFromLonLat(fromLon, fromLat);
            } catch (ServiceException e) {
                e.printStackTrace();
            }

            // Pegar todos os usuarios da nova
            List<String> newUsersIds = null;
            try {
                newUsersIds = UserDAO.getNearbyUsersFromLonLat(toLon, toLat);
            } catch (ServiceException e) {
                e.printStackTrace();
            }

            sendUserAvailablePresences(user.getId(), NEARBY_ROOM, oldUsersIds, newUsersIds);
        }

        if (user.isRegion()) {
            // Pegar todos os usuarios da antiga
            List<String> oldUsersIds = null;
            try {
                oldUsersIds = UserDAO.getRegionUsersFromLonLat(fromLon, fromLat);
            } catch (ServiceException e) {
                e.printStackTrace();
            }

            // Pegar todos os usuarios da nova
            List<String> newUsersIds = null;
            try {
                newUsersIds = UserDAO.getRegionUsersFromLonLat(toLon, toLat);
            } catch (ServiceException e) {
                e.printStackTrace();
            }

            sendUserAvailablePresences(user.getId(), REGION_ROOM, oldUsersIds, newUsersIds);
        }

    }
}
