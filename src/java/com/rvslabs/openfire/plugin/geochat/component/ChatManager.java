package com.rvslabs.openfire.plugin.geochat.component;

import com.rvslabs.openfire.plugin.geochat.dao.UserDAO;
import com.rvslabs.openfire.plugin.geochat.entity.UserModel;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;
import com.rvslabs.openfire.plugin.geochat.listeners.UserLocationListener;
import org.apache.log4j.Logger;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.QName;
import org.jivesoftware.openfire.PacketRouter;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.util.ElementUtil;
import org.xmpp.component.Component;
import org.xmpp.packet.*;

import java.util.ArrayList;
import java.util.List;

public abstract class ChatManager implements UserLocationListener {

    private static final String DOMAIN = "localhost";

    private static Logger Log = Logger.getLogger(ChatManager.class);
    private final PacketRouter packetRouter;
    private Component component;

    protected ChatManager(Component component) {
        this.packetRouter = XMPPServer.getInstance().getPacketRouter();
        this.component = component;
    }

    public void processPacket(Packet packet) throws ServiceException {
        String userId = packet.getFrom().getNode();
        String roomName = packet.getTo().getNode();

        if (packet instanceof Presence) {
            Presence presence = (Presence) packet;

            if (Presence.Type.unsubscribe == presence.getType()) {
                leftUserInRoom(userId, roomName);
            } else if(presence.getType() == null){
                joinUserInRoom(userId, roomName);
            }

        } else if (packet instanceof Message) {
            Message message = (Message) packet;
            processMessage(userId, roomName, message);
        } else if (packet instanceof IQ) {
            IQ iq = (IQ) packet;

            IQ reply = IQ.createResultIQ(iq);

            Element query = packet.getElement().element("query");

            if (query != null && "http://mebble.com/room#users".equals(query.getNamespaceURI())) {
                // TODO: REFACTOR IT GET FULL USER OR CREATE GENERIC METHOD
                List<String> users = this.getOnlineUsers(userId, roomName);

                Element result = reply.setChildElement("query", "http://mebble.com/room#users");
                for (String uID : users) {
                    Element item = result.addElement("item", "http://mebble.com/room#users");
                    UserModel user = UserDAO.getUserWithID(uID);
                    item.addAttribute("name", user.getName());
                    item.addAttribute("id", user.getId());
                    item.addAttribute("image", user.getImageUrl());
                }

                packetRouter.route(reply);
            }

        }
    }

    // Join/LeftRoom
    public void joinUserInRoom(String user, String room) throws ServiceException {

        connectUserToRoom(user, room);

        // Get users Available in Room
        List<String> occupantUsersInRoom = getOnlineUsers(user, room);

        // Service Sends Presence from Existing Occupants to New Occupant
        sendUsersPresenceToUser(user, room, occupantUsersInRoom);

        // Service Sends New Occupant's Presence to All Occupants
        sendUserPresenceToUsers(user, room, occupantUsersInRoom);

        // Send my Presence with Status 110
        sendSelfPresenceToUser(user, room);

        // Send Room Subject to User
        sendRoomSubjectToUser(user, room);
    }

    public void leftUserInRoom(String user, String room) throws ServiceException {
        disconnectUserToRoom(user, room);

        // Service Sends Self-Presence Related to Departure of Occupant
        sendSelfUnavailablePresenceToUser(user, room);

        // Get Users
        List<String> users = getOnlineUsers(user, room);
        users.remove(user);

        // Service Sends Presence Related to Departure of Occupant
        sendUnavailablePresenceToUsers(user, room, users);
    }

    public void sendUserAvailablePresences(String userId, String roomName, List<String> oldUsersIds, List<String> newUsersIds) {
        // Remove o usuario que esta chamando das listas
        oldUsersIds.remove(userId);
        newUsersIds.remove(userId);

        // Pega todos os usúarios que estavam na antiga localização e não estão na nova localização
        List<String> outUsersIds = new ArrayList<>(oldUsersIds);
        outUsersIds.removeAll(newUsersIds);

        // Pega todos os usúarios que estão na nova localização e não estavam na antiga localização
        List<String> inUsersIds = new ArrayList<>(newUsersIds);
        inUsersIds.removeAll(oldUsersIds);

        // Envia para todos os usuarios que eu sai do raio enviando uma presence de unavailable
        Presence myUnavailable = createUnavailablePresenceRoomFromUser(roomName, userId);
        for (String outUserId : outUsersIds) {
            sendPacketToUser(myUnavailable, outUserId);
        }
        // Envia para mim presença de unavailable de todos os usuarios que sairam do raio
        for (String outUserId : outUsersIds) {
            Presence outUserUnavailablePresence = createUnavailablePresenceRoomFromUser(roomName, outUserId);
            sendPacketToUser(outUserUnavailablePresence, userId);
        }

        // Envia para todos os usuarios que eu entrei no raio a minha presença
        Presence myAvailablePresence = createAvailablePresenceRoomFromUser(roomName, userId);
        for (String inUserId : inUsersIds) {
            sendPacketToUser(myAvailablePresence, inUserId);
        }
        // Envia para mim presença de todos usuarios que entraram no raio
        for (String inUserId : inUsersIds) {
            Presence inUserAvailablePresence = createAvailablePresenceRoomFromUser(roomName, inUserId);
            sendPacketToUser(inUserAvailablePresence, userId);
        }
    }

    // Presence Utils
    public void sendUsersPresenceToUser(String user, String room, List<String> usersInRoom) throws ServiceException {
        for (String userId : usersInRoom) {
            if (!userId.equals(user)) {
                Presence presence = createAvailablePresenceRoomFromUser(room, userId);
                sendPacketToUser(presence, user);
            }
        }
    }

    public void sendSelfPresenceToUser(String user, String room) {
        Presence mySelfPresence = createSelfAvailablePresenceRoomFromUser(room, user);
        sendPacketToUser(mySelfPresence, user);
    }

    public void sendSelfUnavailablePresenceToUser(String user, String room) {
        Presence selfUnavailablePresence = createSelfUnavailablePresenceRoomFromUser(room, user);
        sendPacketToUser(selfUnavailablePresence, user);
    }

    public void sendUserPresenceToUsers(String user, String room, List<String> usersRoom) throws ServiceException {
        // Service Sends New Occupant's Presence to All Occupants
        Presence presence = createAvailablePresenceRoomFromUser(room, user);
        for (String userId : usersRoom) {
            if (!userId.equals(user)) {
                sendPacketToUser(presence, userId);
            }
        }
    }

    public void sendUnavailablePresenceToUsers(String user, String room, List<String> usersRoom) {
        Presence unavailablePresence = createUnavailablePresenceRoomFromUser(room, user);
        for (String userId : usersRoom) {
            sendPacketToUser(unavailablePresence, userId);
        }
    }

    public void sendRoomSubjectToUser(String user, String room) {
        Message roomSubject = createRoomSubject(room);
        sendPacketToUser(roomSubject, user);
    }

    // Create Presence Room
    public Presence createAvailablePresenceRoomFromUser(String roomName, String user) {
        return createAvailablePresenceRoomFromUser(roomName, user, false, false);
    }

    public Presence createSelfAvailablePresenceRoomFromUser(String roomName, String user) {
        return createAvailablePresenceRoomFromUser(roomName, user, true, false);

    }

    public Presence createUnavailablePresenceRoomFromUser(String roomName, String user) {
        return createAvailablePresenceRoomFromUser(roomName, user, false, true);
    }

    public Presence createSelfUnavailablePresenceRoomFromUser(String roomName, String user) {
        return createAvailablePresenceRoomFromUser(roomName, user, true, true);
    }

    public Presence createAvailablePresenceRoomFromUser(String roomName, String user, boolean selfPresence, boolean unavailable) {
        Presence presence;
        if (unavailable) {
            presence = new Presence(Presence.Type.unavailable);
        } else {
            presence = new Presence();
        }
        presence.setFrom(new JID(roomName, getComponentDomain(), user));
        Element element = DocumentHelper.createElement(QName.get("x", "http://jabber.org/protocol/muc#user"));
        ElementUtil.setProperty(element, "x.item:jid", new JID(user, "localhost", "").toBareJID());
        ElementUtil.setProperty(element, "x.item:affiliation", "member");
        // TODO: REFACTOR IT
        try {
            UserModel userModel = UserDAO.getUserWithID(user);
            Element userElement = DocumentHelper.createElement(QName.get("x", "http://mebble.com#user"));
            ElementUtil.setProperty(userElement, "x.item:name", userModel.getName());
            ElementUtil.setProperty(userElement, "x.item:image", userModel.getImageUrl());
            presence.getElement().add(userElement);
        } catch (ServiceException e) {
            Log.error("error getting user: " + user);
        }
        if (unavailable) {
            ElementUtil.setProperty(element, "x.item:role", "none");
        } else {
            ElementUtil.setProperty(element, "x.item:role", "participant");
        }
        if (selfPresence) {
            element.addElement("status").addAttribute("code", "110");
            element.addElement("status").addAttribute("code", "100");
        }
        presence.getElement().add(element);
        return presence;
    }

    // Update User Location
//    public void updateUserLocation(String userId, String newLon, String newLat) throws ServiceException {
//        // Get the user to retrieve lon and lat
//        UserModel user = userController.getUser(userId);
//
//        // Store local the old lon and lat
//        String oldLon = user.getLongitude();
//        String oldLat = user.getLatitude();
//
//        // update in database
//        userController.updateUserLocation(user,newLon,newLat);
//
//        if (user.isNearby()) {
//            // Pegar todos os usuarios da antiga
//            List<String> oldUsersIds = userController.getNearbyUsersFromLonLat(oldLon, oldLat);
//
//            // Pegar todos os usuarios da nova
//            List<String> newUsersIds = userController.getNearbyUsersFromLonLat(newLon, newLat);
//
//            sendUserAvailablePresences(userId, NEARBY_ROOM_NAME, oldUsersIds, newUsersIds);
//        }
//
//        if (user.isRegion()) {
//            // Pegar todos os usuarios da antiga
//            List<String> oldUsersIds = userController.getRegionUsersFromLonLat(oldLon, oldLat);
//
//            // Pegar todos os usuarios da nova
//            List<String> newUsersIds = userController.getRegionUsersFromLonLat(newLon, newLat);
//
//            sendUserAvailablePresences(userId, REGION_ROOM_NAME, oldUsersIds, newUsersIds);
//        }
//
//    }

    // Process Message
    public void processMessage(String user, String room, Message message) throws ServiceException {

        // Get Users
        List<String> users = getUsers(user, room);

        // Set Message From Room User
        message.setFrom(new JID(room, getComponentDomain(), user));

        // Service Reflects Message to All Occupants
        for (String userId : users) {
            sendPacketToUser(message, userId);
        }
    }

    public Message createRoomSubject(String roomName) {
        Message roomSubject = new Message();
        roomSubject.setFrom(new JID(roomName, getComponentDomain(), ""));
        roomSubject.setType(Message.Type.groupchat);
        roomSubject.setSubject("");

        return roomSubject;
    }

    // Send packet to user
    public void sendPacketToUser(Packet packet, String user) {
        packet.setTo(new JID(user, DOMAIN, ""));
        packetRouter.route(packet);
    }


    // Abstract Methods
    protected abstract List<String> getUsers(String user, String room) throws ServiceException;

    protected abstract List<String> getOnlineUsers(String user, String room) throws ServiceException;

    protected abstract void connectUserToRoom(String user, String room) throws ServiceException;

    protected abstract void disconnectUserToRoom(String user, String room) throws ServiceException;

    // String Utils
    private String getComponentDomain() {
        return component.getName() + "." + DOMAIN;
    }
}
