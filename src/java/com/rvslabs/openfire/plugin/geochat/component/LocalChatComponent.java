package com.rvslabs.openfire.plugin.geochat.component;

import com.rvslabs.openfire.plugin.geochat.dao.RoomDAO;
import com.rvslabs.openfire.plugin.geochat.entity.UserModel;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;
import com.rvslabs.openfire.plugin.geochat.handlers.UserLocationHandler;
import com.rvslabs.openfire.plugin.geochat.listeners.UserLocationDispatcher;
import com.rvslabs.openfire.plugin.geochat.listeners.UserLocationListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.component.Component;
import org.xmpp.component.ComponentException;
import org.xmpp.component.ComponentManager;
import org.xmpp.packet.JID;
import org.xmpp.packet.Packet;
import org.xmpp.packet.Presence;

/**
 * Created by rvslabs on 20/07/15.
 */
public class LocalChatComponent implements Component {

    private static final Logger Log = LoggerFactory.getLogger(LocalChatComponent.class);

    private ChatManager chatManager;

    @Override
    public String getName() {
        return "localChat";
    }

    @Override
    public String getDescription() {
        return "Chat for locations with Latitude Longitude";
    }

    @Override
    public void processPacket(Packet packet) {
        Log.info("New packet : " + packet.toString());

        try {
            chatManager.processPacket(packet);
        } catch (ServiceException e) {
            Log.error("UserChatComponent error: " + e);
        }

    }

    @Override
    public void initialize(JID jid, ComponentManager componentManager) throws ComponentException {
        this.chatManager = new LocalChatManager(this);
    }

    @Override
    public void start() {
        Log.info("LocalChat component started");
    }

    @Override
    public void shutdown() {
        Log.info("LocalChat component shutdown");
    }

}
