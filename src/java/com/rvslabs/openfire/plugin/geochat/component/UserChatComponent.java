package com.rvslabs.openfire.plugin.geochat.component;

import com.rvslabs.openfire.plugin.geochat.entity.UserModel;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;
import com.rvslabs.openfire.plugin.geochat.listeners.UserLocationDispatcher;
import com.rvslabs.openfire.plugin.geochat.listeners.UserLocationListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.component.Component;
import org.xmpp.component.ComponentException;
import org.xmpp.component.ComponentManager;
import org.xmpp.packet.JID;
import org.xmpp.packet.Message;
import org.xmpp.packet.Packet;
import org.xmpp.packet.Presence;

/**
 * Created by rvslabs on 08/07/15.
 */
public class UserChatComponent implements Component {

    private static final Logger Log = LoggerFactory.getLogger(UserChatComponent.class);

    private ChatManager chatManager;

    public String getName() {
        return "userChat";
    }

    @Override
    public String getDescription() {
        return "Chat for users location based";
    }

    @Override
    public void processPacket(Packet packet) {
        Log.info("New packet : " + packet.toString());
        try {
            chatManager.processPacket(packet);
        } catch (ServiceException e) {
            Log.error("UserChatComponent error: " + e);
        }

    }

    @Override
    public void initialize(JID jid, ComponentManager componentManager) throws ComponentException {
        this.chatManager = new UserChatManager(this);
    }

    @Override
    public void start() {
        Log.info("Nearby component started");
    }

    @Override
    public void shutdown() {
        Log.info("Nearby component shutdown");
    }

}
