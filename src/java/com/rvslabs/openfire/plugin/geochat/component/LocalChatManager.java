package com.rvslabs.openfire.plugin.geochat.component;

import com.rvslabs.openfire.plugin.geochat.dao.RoomDAO;
import com.rvslabs.openfire.plugin.geochat.entity.UserModel;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;
import com.rvslabs.openfire.plugin.geochat.listeners.UserLocationDispatcher;
import org.apache.log4j.Logger;
import org.xmpp.component.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rvslabs on 21/07/15.
 */
public class LocalChatManager extends ChatManager {

    private static Logger Log = Logger.getLogger(LocalChatManager.class);


    protected LocalChatManager(Component component) {
        super(component);
        UserLocationDispatcher.addListener(this);
    }

    @Override
    protected List<String> getUsers(String user, String room) throws ServiceException {
        return RoomDAO.getUsersByRoom(new Long(room));
    }

    @Override
    protected List<String> getOnlineUsers(String user, String room) throws ServiceException {
        return RoomDAO.getUsersOnlineByRoom(new Long(room));
    }

    @Override
    protected void connectUserToRoom(String user, String room) throws ServiceException {
        try {
            RoomDAO.addRoomUser(new Long(room), user);
        } catch (ServiceException e) {
            Log.error("Error adding user to room : " + e);
        }
    }

    @Override
    protected void disconnectUserToRoom(String user, String room) throws ServiceException {
        try {
            RoomDAO.removeRoomUser(new Long(room), user);
        } catch (ServiceException e) {
            Log.error("Error removing user in room : " + e);
        }
    }

    @Override
    public void joinUserInRoom(String user, String room) throws ServiceException {

        connectUserToRoom(user, room);

        List<String> occupantUsersInRoom = RoomDAO.getUsersInRoom(new Long(room));

        // Service Sends Presence from Existing Occupants to New Occupant
        sendUsersPresenceToUser(user, room, occupantUsersInRoom);

        if (occupantUsersInRoom.contains(user)) {
            // Service Sends New Occupant's Presence to All Occupants
            sendUserPresenceToUsers(user, room, getOnlineUsers(user, room));

            // Send my Presence with Status 110
            sendSelfPresenceToUser(user, room);
        }

        // Send Room Subject to User
        sendRoomSubjectToUser(user, room);
    }

    @Override
    public void userChangeLocation(UserModel user, String fromLon, String fromLat, String toLon, String toLat) {

        try {
            // Salas na antiga localização
            List<Long> oldLocationRooms = RoomDAO.getInRoomsUserByLonLatUser(user.getId(), fromLon, fromLat);
            // Sala na nova localização
            List<Long> newLocationRooms = RoomDAO.getInRoomsUserByLonLatUser(user.getId(), toLon, toLat);

            // Salas que ele saiu
            List<Long> quietedRooms = new ArrayList<>(oldLocationRooms);
            quietedRooms.removeAll(newLocationRooms);

            // Salas que ele entrou
            List<Long> newRooms = new ArrayList<>(newLocationRooms);
            newRooms.removeAll(oldLocationRooms);

            // Enviar notificação de unavailable para todos os usuarios da sala que ele saiu (para cada sala)
            for (Long room : quietedRooms) {
                sendUnavailablePresenceToUsers(user.getId(), room.toString(), getOnlineUsers(user.getId(), room.toString()));
                sendSelfUnavailablePresenceToUser(user.getId(), room.toString());
            }

            // Enviar notificação de available para todos os usuarios da sala que ele entrou (para cada sala)
            for (Long room : newRooms) {
                sendUserPresenceToUsers(user.getId(), room.toString(), getOnlineUsers(user.getId(), room.toString()));
                sendSelfPresenceToUser(user.getId(), room.toString());
            }


        } catch (ServiceException e) {
            Log.error("error sending presences: " + e);
        }
    }
}
