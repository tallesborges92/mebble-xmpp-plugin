package com.rvslabs.openfire.plugin.geochat.handlers;

import com.rvslabs.openfire.plugin.geochat.component.ChatManager;
import com.rvslabs.openfire.plugin.geochat.controller.UserController;
import com.rvslabs.openfire.plugin.geochat.dao.UserDAO;
import com.rvslabs.openfire.plugin.geochat.entity.UserModel;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;
import com.rvslabs.openfire.plugin.geochat.listeners.UserLocationDispatcher;
import org.apache.log4j.Logger;
import org.jivesoftware.openfire.IQHandlerInfo;
import org.jivesoftware.openfire.auth.UnauthorizedException;
import org.jivesoftware.openfire.handler.IQHandler;
import org.xmpp.packet.*;
import org.dom4j.Element;

/**
 * Created by rvslabs on 02/07/15.
 */
public class UserLocationHandler extends IQHandler {

    private static Logger Log = Logger.getLogger(UserLocationHandler.class);

    private static final String NAME_SPACE = "com.rvslabs.location";
    private IQHandlerInfo info;

    /**
     * Create a basic module with the given name.
     *
     * @param moduleName The name for the module or null to use the default
     */
    public UserLocationHandler(String moduleName) {
        super(moduleName);
        info = new IQHandlerInfo(moduleName, NAME_SPACE);
    }

    @Override
    public IQ handleIQ(IQ packet) throws UnauthorizedException {
        Log.info(packet);
        if (IQ.Type.set.equals(packet.getType())) {

            JID from = packet.getFrom();
            String userId = packet.getFrom().getNode();

            Element iq = packet.getChildElement();
            Element item = iq.element("item");

            String newLon = item.attributeValue("lon");
            String newLat = item.attributeValue("lat");

            try {
                UserModel user = UserDAO.getUserWithID(userId);

                String oldLon = user.getLongitude();
                String oldLat = user.getLatitude();

                UserDAO.updateLocation(userId, newLon, newLat);

                UserLocationDispatcher.dispatchUserLocationChanged(user, oldLon, oldLat, newLon, newLat);

            } catch (ServiceException e) {
                return createErrorPacket(packet);
            }

            return IQ.createResultIQ(packet);
        } else {
            return createErrorPacket(packet);
        }
    }

    public IQ createErrorPacket(IQ packet) {
        IQ reply = IQ.createResultIQ(packet);
        reply.setType(IQ.Type.error);
        reply.setError(PacketError.Condition.bad_request);
        return reply;
    }

    @Override
    public IQHandlerInfo getInfo() {
        return info;
    }
}
