package com.rvslabs.openfire.plugin.geochat.handlers;

import com.rvslabs.openfire.plugin.geochat.dao.RoomDAO;
import com.rvslabs.openfire.plugin.geochat.entity.Room;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.QName;
import org.dom4j.tree.DefaultElement;
import org.jivesoftware.openfire.IQHandlerInfo;
import org.jivesoftware.openfire.auth.UnauthorizedException;
import org.jivesoftware.openfire.handler.IQHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.packet.IQ;
import org.xmpp.packet.PacketError;

import java.util.List;

/**
 * Created by rvslabs on 24/07/15.
 */
public class RoomHandler extends IQHandler {

    private static final Logger Log = LoggerFactory.getLogger(RoomHandler.class);

    private static final String NAME_SPACE = "mebble:iq:room";
    private IQHandlerInfo info;

    /**
     * Create a basic module with the given name.
     *
     * @param moduleName The name for the module or null to use the default
     */
    public RoomHandler(String moduleName) {
        super(moduleName);
        info = new IQHandlerInfo(moduleName, NAME_SPACE);
    }

    @Override
    public IQ handleIQ(IQ packet) throws UnauthorizedException {

        Log.info(packet.toString());

        if (IQ.Type.get.equals(packet.getType())) {

            Element item = packet.getChildElement().element("item");

            String category = item.attributeValue("category");
            try {
                String userId = packet.getFrom().getNode();
                switch (category) {
                    case "created":
                        return createResultListFromPacket(packet, RoomDAO.getRoomsCreatedByUser(userId));
                    case "participating":
                        return createResultListFromPacket(packet, RoomDAO.getRoomsParticipatingByUser(userId));
                    case "suggestion":
                        return createResultListFromPacket(packet, RoomDAO.getRoomsSuggestionToUser(userId));
                    default:
                        return createErrorPacket(packet);
                }
            } catch (ServiceException e) {
                return createErrorPacket(packet);
            }
        } else if (IQ.Type.set.equals(packet.getType())) {

            Element query = packet.getChildElement();

            Element form = query.element(QName.get("x", "mebble:x:room_form"));

            String name = form.element("name").getStringValue();
            String description = form.element("description").getStringValue();
            String range = form.element("range").getStringValue();
            String latitude = form.element("latitude").getStringValue();
            String longitude = form.element("longitude").getStringValue();
            String addressDescription = form.element("addressDescription").getStringValue();
            String urlImage = form.element("urlImage").getStringValue();


            Room room = new Room();
            room.setName(name);
            room.setDescription(description);
            room.setRange(new Integer(range));
            room.setLatitude(latitude);
            room.setLongitude(longitude);
            room.setAddressDescription(addressDescription);
            room.setUrlImage(urlImage);
            room.setCreatorId(packet.getFrom().getNode());

            try {
                Room roomCreated = RoomDAO.createRoom(room);

                IQ resultIQ = IQ.createResultIQ(packet);
                Element queryElement = DocumentHelper.createElement(QName.get("query", NAME_SPACE));
                Element roomElement = createElementRoom(roomCreated);
                queryElement.add(roomElement);
                resultIQ.getElement().add(queryElement);

                return resultIQ;

            } catch (ServiceException e) {
                return createErrorPacket(packet);
            }
        }

        return createErrorPacket(packet);
    }

    public Element createElementRoom(Room room) {
        Element roomElement = new DefaultElement("item");
        roomElement.addAttribute("id", String.valueOf(room.getId()));
        roomElement.addAttribute("name", room.getName());
        roomElement.addAttribute("description", room.getDescription());
        roomElement.addAttribute("range", String.valueOf(room.getRange()));
        roomElement.addAttribute("longitude", room.getLongitude());
        roomElement.addAttribute("latitude", room.getLatitude());
        roomElement.addAttribute("address_description", room.getAddressDescription());
        roomElement.addAttribute("url_image", room.getUrlImage());
        return roomElement;
    }


    public IQ createResultListFromPacket(IQ packet, List<Room> rooms) {
        IQ resultIQ = IQ.createResultIQ(packet);
        Element queryElement = DocumentHelper.createElement(QName.get("query", NAME_SPACE));

        for (Room room : rooms) {
            Element roomElement = createElementRoom(room);
            queryElement.add(roomElement);
        }

        resultIQ.getElement().add(queryElement);

        return resultIQ;
    }

    public IQ createErrorPacket(IQ packet) {
        IQ reply = IQ.createResultIQ(packet);
        reply.setType(IQ.Type.error);
        reply.setError(PacketError.Condition.bad_request);
        return reply;
    }

    @Override
    public IQHandlerInfo getInfo() {
        return info;
    }
}
