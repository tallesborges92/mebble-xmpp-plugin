package com.rvslabs.openfire.plugin.geochat.handlers;

import com.rvslabs.openfire.plugin.geochat.dao.UserDAO;
import com.rvslabs.openfire.plugin.geochat.entity.UserModel;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.QName;
import org.jivesoftware.openfire.IQHandlerInfo;
import org.jivesoftware.openfire.auth.UnauthorizedException;
import org.jivesoftware.openfire.handler.IQHandler;
import org.xmpp.packet.IQ;
import org.xmpp.packet.PacketError;

/**
 * Created by rvslabs on 28/07/15.
 */
public class UserInfoHandler extends IQHandler {


    private static final String NAME_SPACE = "com.rvslabs.user";
    private IQHandlerInfo info;

    /**
     * Create a basic module with the given name.
     *
     * @param moduleName The name for the module or null to use the default
     */
    public UserInfoHandler(String moduleName) {
        super(moduleName);
        info = new IQHandlerInfo(moduleName, NAME_SPACE);
    }

    @Override
    public IQ handleIQ(IQ packet) throws UnauthorizedException {

        try {
            IQ resultIQ = IQ.createResultIQ(packet);

            Element iq = packet.getChildElement();
            Element item = iq.element("item");

            String userId = item.attributeValue("userId");

            UserModel userModel = UserDAO.getUserWithID(userId);

            Element userElement = DocumentHelper.createElement("user");

            userElement.addAttribute("id", String.valueOf(userModel.getId()));
            userElement.addAttribute("name", userModel.getName());
            userElement.addAttribute("image", userModel.getImageUrl());

            Element queryElement = DocumentHelper.createElement(QName.get("query", NAME_SPACE));

            queryElement.add(userElement);

            resultIQ.getElement().add(queryElement);

            return resultIQ;

        } catch (ServiceException e) {
            return createErrorPacket(packet);
        }
    }

    @Override
    public IQHandlerInfo getInfo() {
        return info;
    }

    public IQ createErrorPacket(IQ packet) {
        IQ reply = IQ.createResultIQ(packet);
        reply.setType(IQ.Type.error);
        reply.setError(PacketError.Condition.bad_request);
        return reply;
    }
}
