package com.rvslabs.openfire.plugin.geochat.service;

import com.rvslabs.openfire.plugin.geochat.controller.AuthenticationController;
import com.rvslabs.openfire.plugin.geochat.entity.PhoneNumber;
import com.rvslabs.openfire.plugin.geochat.entity.UserModel;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Created by rvslabs on 25/06/15.
 */
@Path("geochat/v1/authentication")
public class AuthenticationService {

    private static Logger Log = Logger.getLogger(AuthenticationService.class);

    private AuthenticationController controller;

    @PostConstruct
    public void init() {
        controller = AuthenticationController.getInstance();
    }

    @POST
    @Path("/authenticate")
    public Response authenticatePhone(@FormParam("ddi") String ddi, @FormParam("number") String number) throws ServiceException {
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setDdi(ddi);
        phoneNumber.setNumber(number);
        UserModel userModel = controller.getOrCreateUserWithPhone(phoneNumber);

        controller.sendSMSAuthenticationToUser(userModel);

        return Response.status(Response.Status.OK).build();
    }

    @POST
    @Path("/verify")
    @Produces("application/json")
    public Response authenticateCode(@FormParam("code") String code, @FormParam("ddi") String ddi, @FormParam("number") String number) throws ServiceException {
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setDdi(ddi);
        phoneNumber.setNumber(number);
        UserModel userModel = controller.getUserWithPhone(phoneNumber);

        controller.validateCode(code, userModel);

        return Response.ok(userModel).build();
    }

}
