package com.rvslabs.openfire.plugin.geochat.service;

import com.rvslabs.openfire.plugin.geochat.dao.RoomDAO;
import com.rvslabs.openfire.plugin.geochat.dao.UserDAO;
import com.rvslabs.openfire.plugin.geochat.entity.Room;
import com.rvslabs.openfire.plugin.geochat.entity.UserModel;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;
import com.rvslabs.openfire.plugin.geochat.response.RoomResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by rvslabs on 17/07/15.
 */
@Path("geochat/v1/rooms")
public class RoomService {

    private static Logger LOG = LoggerFactory.getLogger(RoomService.class);


    @POST
    public Response authenticatePhone(@FormParam("userId") String userId,
                                      @FormParam("userPwd") String userPwd,
                                      @FormParam("name") String name,
                                      @FormParam("description") String description,
                                      @FormParam("range") Integer range,
                                      @FormParam("latitude") String latitude,
                                      @FormParam("longitude") String longitude,
                                      @FormParam("addressDescription") String addressDescription,
                                      @FormParam("urlImage") String urlImage) throws ServiceException {

        UserModel user = UserDAO.getUserWithIdPwd(userId, userPwd);

        Room room = new Room();
        room.setName(name);
        room.setDescription(description);
        room.setRange(range);
        room.setLatitude(latitude);
        room.setLongitude(longitude);
        room.setAddressDescription(addressDescription);
        room.setUrlImage(urlImage);

        Room roomCreated = RoomDAO.createRoom(room);

        RoomDAO.addRoomUser(roomCreated.getId(), userId);

        LOG.info("Room created " + roomCreated.getId());

        return Response.ok(new RoomResponse(roomCreated), MediaType.APPLICATION_JSON_TYPE).build();
    }
}
