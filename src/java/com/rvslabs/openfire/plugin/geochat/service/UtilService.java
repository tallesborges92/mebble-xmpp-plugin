package com.rvslabs.openfire.plugin.geochat.service;


import com.rvslabs.openfire.plugin.geochat.controller.UtilController;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;

import javax.annotation.PostConstruct;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Created by rvslabs on 01/07/15.
 */

@Path("geochat/v1/util")
public class UtilService {

    private UtilController controller;

    @PostConstruct
    public void init() {
        controller = UtilController.getInstance();
    }

    @GET
    @Path("/countries")
    @Produces("application/json")
    public Response getCountries() throws ServiceException {
        return Response.ok(controller.getCountries()).build();
    }

}
