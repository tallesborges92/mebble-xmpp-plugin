package com.rvslabs.openfire.plugin.geochat;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;

/**
 * The Class AuthFilter.
 */
public class AuthFilter implements ContainerRequestFilter {

	/** The log. */
	private static Logger LOG = LoggerFactory.getLogger(AuthFilter.class);

	/** The http request. */
	@Context
	private HttpServletRequest httpRequest;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sun.jersey.spi.container.ContainerRequestFilter#filter(com.sun.jersey
	 * .spi.container.ContainerRequest)
	 */
	@Override
	public ContainerRequest filter(ContainerRequest containerRequest) throws WebApplicationException {
		return containerRequest;
	}
}
