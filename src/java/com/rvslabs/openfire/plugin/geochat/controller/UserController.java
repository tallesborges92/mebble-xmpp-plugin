package com.rvslabs.openfire.plugin.geochat.controller;

import com.rvslabs.openfire.plugin.geochat.dao.UserDAO;
import com.rvslabs.openfire.plugin.geochat.entity.UserModel;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;
import com.rvslabs.openfire.plugin.geochat.handlers.UserLocationHandler;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rvslabs on 02/07/15.
 */
public class UserController {

    private static Logger Log = Logger.getLogger(UserLocationHandler.class);
    private static final UserController INSTANCE = new UserController();

    public static UserController getInstance() {
        return INSTANCE;
    }

    // Get User From ID
    public UserModel getUser(String userId) throws ServiceException {
        return UserDAO.getUserWithID(userId);
    }

    // Update Location
    public void updateUserLocation(String userId, String longitude, String latitude) throws ServiceException {
        UserDAO.updateLocation(userId,longitude,latitude);
    }


    // Connect To Room
    public boolean connectUserToRoom(String userId, String room) {
        return connectUserToRoom(userId, room, true);
    }

    public boolean disconnectUserToRoom(String userId, String room) {
        return connectUserToRoom(userId, room, false);
    }

    public boolean connectUserToRoom(String userId, String room, boolean connect) {
        if (room.equals("nearby")) {
            return connectUserToNearbyRoom(userId, connect);
        } else if (room.equals("region")) {
            return connectUserToRegionRoom(userId, connect);
        }
        return false;
    }

    public boolean connectUserToRegionRoom(String userId, boolean connect) {
        try {
            UserDAO.connectToRegionChat(userId, connect);
        } catch (ServiceException e) {
            Log.error(e);
            return false;
        }

        return true;
    }

    public boolean connectUserToNearbyRoom(String userId, boolean connect) {
        try {
            UserDAO.connectUserToNearbyChat(userId, connect);
        } catch (ServiceException e) {
            Log.error(e);
            return false;
        }

        return true;
    }

    // Set Usem Online
    public boolean setUserOnline(String user, boolean status) {
        try {
            UserDAO.setUserOnline(user, status);
        } catch (ServiceException e) {
            Log.error(e);
            return false;
        }
        return true;
    }

    // Get Users From User Location
    public List<String> getNearbyUsersFromUser(String user) {
        List<String> nearbyUsersFromUser;

        try {
            nearbyUsersFromUser = UserDAO.getNearbyUsersFromUser(user);
        } catch (ServiceException e) {
            Log.error(e);
            nearbyUsersFromUser = new ArrayList<>();
        }

        return nearbyUsersFromUser;
    }

    public List<String> getRegionUsersFromUser(String user) {
        List<String> regionUsersFromUser;

        try {
            regionUsersFromUser = UserDAO.getRegionUsersFromUser(user);
        } catch (ServiceException e) {
            Log.error(e);
            regionUsersFromUser = new ArrayList<>();
        }

        return regionUsersFromUser;
    }

    public List<String> getRoomUsersFromUser(String user, String room) {

        List<String> users = null;
        if (room.equals("nearby")) {
            users = getNearbyUsersFromUser(user);
        } else if (room.equals("region")) {
            users = getRegionUsersFromUser(user);
        }

        return users;
    }

    // Get Users From Lat Lon
    public List<String> getRegionUsersFromLonLat(String lon, String lat) {
        return getUsersFromLonLatByRoom(lon, lat, "region");
    }

    public List<String> getNearbyUsersFromLonLat(String lon, String lat) {
        return getUsersFromLonLatByRoom(lon, lat, "nearby");
    }

    public List<String> getUsersFromLonLatByRoom(String lon, String lat, String room) {
        try {
            if (room.equals("nearby")) {
                return UserDAO.getNearbyUsersFromLonLat(lon, lat);
            } else if (room.equals("region")) {
                return UserDAO.getRegionUsersFromLonLat(lon, lat);
            }
        } catch (ServiceException e) {
            Log.error(e);
        }
        return new ArrayList<>();
    }
}
