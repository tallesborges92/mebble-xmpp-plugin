package com.rvslabs.openfire.plugin.geochat.controller;


import com.rvslabs.openfire.plugin.geochat.dao.UtilDAO;
import com.rvslabs.openfire.plugin.geochat.entity.CountryEntity;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;
import com.rvslabs.openfire.plugin.geochat.response.CountriesResponse;

import java.util.List;

/**
 * Created by rvslabs on 01/07/15.
 */
public class UtilController {

    private static final UtilController INSTANCE = new UtilController();

    public static UtilController getInstance() {
        return INSTANCE;
    }

    public CountriesResponse getCountries() throws ServiceException {
        List<CountryEntity> countries = UtilDAO.getCountries();

        CountriesResponse response = new CountriesResponse();

        response.setCountries(countries);

        return response;
    }

}
