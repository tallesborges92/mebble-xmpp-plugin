package com.rvslabs.openfire.plugin.geochat.controller;

import com.rvslabs.openfire.plugin.geochat.dao.UserDAO;
import com.rvslabs.openfire.plugin.geochat.entity.AuthenticationEntity;
import com.rvslabs.openfire.plugin.geochat.entity.PhoneNumber;
import com.rvslabs.openfire.plugin.geochat.entity.UserModel;
import com.rvslabs.openfire.plugin.geochat.exceptions.InvalidPhoneException;
import com.rvslabs.openfire.plugin.geochat.exceptions.InvalidAuthenticationException;
import com.rvslabs.openfire.plugin.geochat.exceptions.PhoneNotFoundException;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;
import com.rvslabs.openfire.plugin.geochat.utils.SMSUtil;
import org.jivesoftware.openfire.XMPPServer;
import org.joda.time.DateTime;

import java.util.Random;

/**
 * Created by rvslabs on 29/06/15.
 */
public class AuthenticationController {

    public static final AuthenticationController INSTANCE = new AuthenticationController();
    private XMPPServer server;


    public static AuthenticationController getInstance() {
        return INSTANCE;
    }

    public UserModel getUserWithPhone(PhoneNumber phoneNumber) throws ServiceException {
        return UserDAO.getUserWithPhone(phoneNumber);
    }

    public AuthenticationController() {
        server = XMPPServer.getInstance();
    }

    public UserModel createUserWithPhone(PhoneNumber phoneNumber) throws ServiceException {
        if (phoneNumber.getDdi().isEmpty() || phoneNumber.getNumber().isEmpty()) {
            throw new InvalidPhoneException();
        }

        return UserDAO.createUser(phoneNumber);
    }

    public UserModel getOrCreateUserWithPhone(PhoneNumber phoneNumber) throws ServiceException {
        UserModel userModel = null;

        try {
            userModel = getUserWithPhone(phoneNumber);
        } catch (PhoneNotFoundException ex) {
            userModel = createUserWithPhone(phoneNumber);
        }

        return userModel;
    }

    public void sendSMSAuthenticationToUser(UserModel userModel) throws ServiceException {
        AuthenticationEntity authenticationEntity = new AuthenticationEntity();

        Random random = new Random();
        int code = random.nextInt(10000);

        authenticationEntity.setUserModel(userModel);
        authenticationEntity.setCode("" + code);
        authenticationEntity.setSendDate(DateTime.now());
        authenticationEntity.setExpirationDate(DateTime.now().plusMinutes(5));

        UserDAO.saveAuthentication(authenticationEntity);

        SMSUtil.sendAuthenticationSMS(authenticationEntity);
    }

    public void validateCode(String code, UserModel userModel) throws ServiceException {

        Boolean isValid = UserDAO.isValidCodeForUser(code, userModel);

        if(!isValid)
            throw new InvalidAuthenticationException();

    }
}
