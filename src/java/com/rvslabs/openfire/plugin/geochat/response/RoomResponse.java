package com.rvslabs.openfire.plugin.geochat.response;

import com.rvslabs.openfire.plugin.geochat.entity.Room;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by rvslabs on 17/07/15.
 */
@XmlRootElement
public class RoomResponse {

    private long id;
    private String name;
    private String description;
    private long range;
    private String latitude;
    private String longitude;
    private String addressDescription;
    private String urlImage;

    public RoomResponse() {
    }

    public RoomResponse(Room room) {
        this.id = room.getId();
        this.name = room.getName();
        this.description = room.getDescription();
        this.range = room.getRange();
        this.latitude = room.getLatitude();
        this.longitude = room.getLongitude();
        this.addressDescription = room.getAddressDescription();
        this.urlImage = room.getUrlImage();
    }

    @XmlElement
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlElement
    public long getRange() {
        return range;
    }

    public void setRange(long range) {
        this.range = range;
    }

    @XmlElement
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @XmlElement
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @XmlElement
    public String getAddressDescription() {
        return addressDescription;
    }

    public void setAddressDescription(String addressDescription) {
        this.addressDescription = addressDescription;
    }

    @XmlElement
    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }
}
