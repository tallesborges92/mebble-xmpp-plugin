package com.rvslabs.openfire.plugin.geochat.response;

import com.rvslabs.openfire.plugin.geochat.entity.CountryEntity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by rvslabs on 01/07/15.
 */

@XmlRootElement
public class CountriesResponse {

    private List<CountryEntity> countries;

    @XmlElement
    public List<CountryEntity> getCountries() {
        return countries;
    }

    public void setCountries(List<CountryEntity> countries) {
        this.countries = countries;
    }
}
