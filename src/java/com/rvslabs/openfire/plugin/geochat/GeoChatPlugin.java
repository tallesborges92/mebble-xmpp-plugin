package com.rvslabs.openfire.plugin.geochat;

import com.rvslabs.openfire.plugin.geochat.component.LocalChatComponent;
import com.rvslabs.openfire.plugin.geochat.component.UserChatComponent;
import com.rvslabs.openfire.plugin.geochat.handlers.RoomHandler;
import com.rvslabs.openfire.plugin.geochat.handlers.UserInfoHandler;
import com.rvslabs.openfire.plugin.geochat.handlers.UserLocationHandler;
import com.rvslabs.openfire.plugin.geochat.listeners.GeoChatSessionListenerImpl;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.container.Plugin;
import org.jivesoftware.openfire.container.PluginManager;
import org.jivesoftware.openfire.event.SessionEventDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.component.ComponentException;
import org.xmpp.component.ComponentManagerFactory;

import java.io.File;

/**
 * Created by rvslabs on 02/07/15.
 */
public class GeoChatPlugin  implements Plugin {

    private static final Logger Log = LoggerFactory.getLogger(GeoChatPlugin.class);

    /** The Constant INSTANCE. */
    public static final GeoChatPlugin INSTANCE = new GeoChatPlugin();

    private static final String MODULE_NAME_LOCATION = "location";
    private static final String MODULE_NAME_ROOM = "room";
    private static final String MODULE_NAME_USER = "user";


    private static final String USERCHAT_COMPONENT = "userChat";
    private static final String LOCALCHAT_COMPONENT = "localChat";

    private XMPPServer server;

    private UserLocationHandler userLocationHandler;
    private RoomHandler roomHandler;

    private UserChatComponent userChatComponent;
    private LocalChatComponent localChatComponent;

    private GeoChatSessionListenerImpl sessionListener;
    private UserInfoHandler userInfoHandler;

    public static GeoChatPlugin getInstance() {
        return INSTANCE;
    }

    @Override
    public void initializePlugin(PluginManager manager, File pluginDirectory) {
        server = XMPPServer.getInstance();

        // Compoments
        userChatComponent = new UserChatComponent();
        localChatComponent = new LocalChatComponent();
        // Add Components
        try {
            ComponentManagerFactory.getComponentManager().addComponent(USERCHAT_COMPONENT, userChatComponent);
        } catch (ComponentException e) {
            Log.error("Error initializing UserChatComponent", e);
        }

        try {
            ComponentManagerFactory.getComponentManager().addComponent(LOCALCHAT_COMPONENT, localChatComponent);
        } catch (ComponentException e) {
            Log.error("Error initializing LocalChatComponent", e);
        }

        // Handlers
        userLocationHandler = new UserLocationHandler(MODULE_NAME_LOCATION);
        roomHandler = new RoomHandler(MODULE_NAME_ROOM);
        userInfoHandler = new UserInfoHandler(MODULE_NAME_USER);

        // Add Handlers
        server.getIQRouter().addHandler(userLocationHandler);
        server.getIQRouter().addHandler(roomHandler);
        server.getIQRouter().addHandler(userInfoHandler);

        // Listener
        sessionListener = new GeoChatSessionListenerImpl();
        // Add Listeners
        SessionEventDispatcher.addListener(sessionListener);

    }

    @Override
    public void destroyPlugin() {

        if (userLocationHandler != null) {
            server.getIQRouter().removeHandler(userLocationHandler);
            userLocationHandler = null;
        }

        if (roomHandler != null) {
            server.getIQRouter().removeHandler(roomHandler);
            roomHandler = null;
        }

        // Remove Session Handler
        SessionEventDispatcher.removeListener(sessionListener);
        sessionListener = null;

        try {
            ComponentManagerFactory.getComponentManager().removeComponent(USERCHAT_COMPONENT);
        } catch (ComponentException e) {
            Log.error("Error stopping NearbyComponent", e);
        }

        try {
            ComponentManagerFactory.getComponentManager().removeComponent(LOCALCHAT_COMPONENT);
        } catch (ComponentException e) {
            Log.error("Error stopping LocalChatComponent", e);
        }
    }
}
