package com.rvslabs.openfire.plugin.geochat.exceptions;

import com.rvslabs.openfire.plugin.geochat.utils.ExceptionType;

import javax.ws.rs.core.Response;

/**
 * Created by rvslabs on 02/07/15.
 */
public class InvalidPhoneException extends ServiceException {

    public InvalidPhoneException() {
        super(ExceptionType.PHONE_INVALID_MESSAGE,ExceptionType.PHONE_INVALID_CODE, Response.Status.BAD_REQUEST);
    }


    public InvalidPhoneException(String msg, int code, Response.Status status, Throwable cause) {
        super(msg, code, status, cause);
    }

    public InvalidPhoneException(String msg, int code, Response.Status status) {
        super(msg, code, status);
    }
}
