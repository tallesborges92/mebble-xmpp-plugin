package com.rvslabs.openfire.plugin.geochat.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response.Status;

/**
 * The Class ServiceException.
 */
public class ServiceException extends Exception {

    private static final Logger Log = LoggerFactory.getLogger(ServiceException.class);


    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 4351720088030656859L;

    private int code;

    private Status status;

    public ServiceException(String msg, int code, Status status) {
        super(msg);
        this.code = code;
        this.status = status;

        Log.error("Exception : " + status);
    }

    public ServiceException(String msg, int code, Status status, Throwable cause) {
        super(msg, cause);
        this.code = code;
        this.status = status;

        Log.error("Exception : " + cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
        this.code = 666;
        this.status = Status.BAD_REQUEST;

        Log.error("Exception : " + cause);
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}