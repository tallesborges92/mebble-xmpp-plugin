package com.rvslabs.openfire.plugin.geochat.exceptions;

import com.rvslabs.openfire.plugin.geochat.response.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * The Class RESTExceptionMapper.
 */
@Provider
public class RESTExceptionMapper implements ExceptionMapper<ServiceException> {

	/** The log. */
	private static Logger LOG = LoggerFactory.getLogger(RESTExceptionMapper.class);

	/**
	 * Instantiates a new REST exception mapper.
	 */
	public RESTExceptionMapper() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.ws.rs.ext.ExceptionMapper#toResponse(java.lang.Throwable)
	 */
	public Response toResponse(ServiceException exception) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setCode(exception.getCode());
		errorResponse.setMessage(exception.getMessage());

		LOG.error(exception.toString());

		return Response.status(exception.getStatus()).entity(errorResponse).type(MediaType.APPLICATION_JSON).build();
	}

}
