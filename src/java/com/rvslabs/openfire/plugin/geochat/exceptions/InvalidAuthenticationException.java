package com.rvslabs.openfire.plugin.geochat.exceptions;

import com.rvslabs.openfire.plugin.geochat.utils.ExceptionType;

import javax.ws.rs.core.Response;

/**
 * Created by rvslabs on 02/07/15.
 */
public class InvalidAuthenticationException extends  ServiceException {

    public InvalidAuthenticationException() {
        super(ExceptionType.CODE_INVALID_MESSAGE, ExceptionType.CODE_INVALID_CODE, Response.Status.BAD_REQUEST);
    }

    public InvalidAuthenticationException(String msg, int code, Response.Status status) {
        super(msg, code, status);
    }

    public InvalidAuthenticationException(String msg, int code, Response.Status status, Throwable cause) {
        super(msg, code, status, cause);
    }
}
