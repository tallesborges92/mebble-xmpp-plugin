package com.rvslabs.openfire.plugin.geochat.exceptions;

import com.rvslabs.openfire.plugin.geochat.utils.ExceptionType;

import javax.ws.rs.core.Response;

/**
 * Created by rvslabs on 02/07/15.
 */
public class PhoneNotFoundException extends ServiceException {

    public PhoneNotFoundException() {
        super(ExceptionType.NUMBER_NOT_FOUND_MESSAGE, ExceptionType.NUMBER_NOT_FOUND_CODE, Response.Status.BAD_REQUEST);
    }

    public PhoneNotFoundException(String msg, int code, Response.Status status) {
        super(msg, code, status);
    }

    public PhoneNotFoundException(String msg, int code, Response.Status status, Throwable cause) {
        super(msg, code, status, cause);
    }
}
