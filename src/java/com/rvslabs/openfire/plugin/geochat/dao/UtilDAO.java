package com.rvslabs.openfire.plugin.geochat.dao;

import com.rvslabs.openfire.plugin.geochat.entity.CountryEntity;
import com.rvslabs.openfire.plugin.geochat.utils.ExceptionType;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;
import org.jivesoftware.database.DbConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rvslabs on 01/07/15.
 */
public class UtilDAO {

    private static Logger LOG = LoggerFactory.getLogger(UtilDAO.class);


    private final static String SELECT_CONTRIES = "SELECT name, callingcode, alpha2code, alpha3code FROM public.country";

    public static List<CountryEntity> getCountries() throws ServiceException {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<CountryEntity> countries = new ArrayList<>();
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(SELECT_CONTRIES);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                CountryEntity countryEntity = new CountryEntity();

                countryEntity.setName(rs.getString(1));
                countryEntity.setCallingCode(rs.getString(2));
                countryEntity.setAlpha2Code(rs.getString(3));
                countryEntity.setAlpha3Code(rs.getString(4));

                countries.add(countryEntity);
            }

        } catch (SQLException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(rs,pstmt, con);
        }

        return countries;
    }

}
