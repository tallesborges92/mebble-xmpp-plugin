package com.rvslabs.openfire.plugin.geochat.dao;

import com.rvslabs.openfire.plugin.geochat.entity.AuthenticationEntity;
import com.rvslabs.openfire.plugin.geochat.entity.PhoneNumber;
import com.rvslabs.openfire.plugin.geochat.entity.UserModel;
import com.rvslabs.openfire.plugin.geochat.exceptions.PhoneNotFoundException;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;
import com.rvslabs.openfire.plugin.geochat.utils.PostgisUtil;
import com.rvslabs.openfire.plugin.geochat.utils.UserUtils;
import org.jivesoftware.database.DbConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rvslabs on 29/06/15.
 */
public class UserDAO {

    private static Logger LOG = LoggerFactory.getLogger(UserDAO.class);

    private final static String CREATE_USER = "INSERT INTO rvsuser (id, password, phone_ddi, phone_number) VALUES(?, ?, ?, ?)";
    private final static String SELECT_ID_PASSWORD_FROM_USER = "SELECT id, password FROM rvsuser WHERE phone_ddi = ? AND phone_number = ?";
    private final static String SELECT_USER_BY_ID = "SELECT id, password, name, phone_ddi, phone_number, email, ST_X(location::GEOMETRY), ST_Y(location::GEOMETRY), nearby, region, image_url, online FROM rvsuser WHERE id = ?";
    private final static String SELECT_USER_BY_ID_PWD = "SELECT id, password, name, phone_ddi, phone_number, email, ST_X(location::GEOMETRY), ST_Y(location::GEOMETRY), nearby, region, online, image_url FROM rvsuser WHERE id = ? and password = ?";
    private final static String UPDATE_USER_LOCATION = "UPDATE rvsuser SET location=ST_GeographyFromText(?) WHERE id= ?";
    private final static String SAVE_AUTHENTICATION = "INSERT INTO confirmation (user_id, code, send_date, expiration_date) VALUES(?, ?, ?, ?)";
    private final static String VALIDATE_CODE = "SELECT user_id FROM public.confirmation WHERE code = ? AND NOW() < expiration_date AND user_id = ?";

    // UserChat
    private final static String CONNECT_USER_NEARBY = "UPDATE rvsuser SET nearby=? WHERE id=?";
    private final static String CONNECT_USER_REGION = "UPDATE rvsuser SET region=? WHERE id=?";
    private final static String SELECT_NEARBY_IDS = "SELECT u.id FROM rvsuser AS u WHERE ST_DWithin(u.location, (SELECT location FROM rvsuser WHERE id = ?), 1000, FALSE) AND u.nearby=TRUE";
    private final static String SELECT_ONLINE_NEARBY_IDS = "SELECT u.id FROM rvsuser AS u WHERE ST_DWithin(u.location, (SELECT location FROM rvsuser WHERE id = ?), 1000, FALSE) AND u.nearby=TRUE AND u.online=TRUE";
    private final static String SELECT_REGION_IDS = "SELECT u.id FROM rvsuser AS u WHERE ST_DWithin(u.location, (SELECT location FROM rvsuser WHERE id = ?), 2500, FALSE) AND u.region=TRUE";
    private final static String SELECT_ONLINE_REGION_IDS = "SELECT u.id FROM rvsuser AS u WHERE ST_DWithin(u.location, (SELECT location FROM rvsuser WHERE id = ?), 2500, FALSE) AND u.region=TRUE AND u.online=TRUE";
    private final static String SELECT_NEARBY_IDS_BY_LON_LAT = "SELECT u.id FROM rvsuser AS u WHERE ST_DWithin(u.location, ST_GeographyFromText(?), 100, FALSE) AND u.nearby=TRUE AND u.online=TRUE";
    private final static String SELECT_REGION_IDS_BY_LON_LAT = "SELECT u.id FROM rvsuser AS u WHERE ST_DWithin(u.location, ST_GeographyFromText(?), 1000, FALSE) AND u.region=TRUE AND u.online=TRUE";
    private final static String SET_USER_ONLINE = "UPDATE rvsuser SET online=? WHERE id=?";

    public static UserModel createUser(PhoneNumber phoneNumber) throws ServiceException {
        UserModel userModel = new UserModel();

        userModel.setId(UserUtils.UUIDWithoutDashes());
        userModel.setPassword(UserUtils.UUIDWithoutDashes());

        userModel.setDdi(phoneNumber.getDdi());
        userModel.setNumber(phoneNumber.getNumber());

        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(CREATE_USER);

            pstmt.setString(1, userModel.getId());
            pstmt.setString(2, userModel.getPassword());
            pstmt.setString(3, userModel.getDdi());
            pstmt.setString(4, userModel.getNumber());

            pstmt.execute();

        } catch (SQLException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(pstmt, con);
        }

        return userModel;
    }

    public static UserModel getUserWithIdPwd(String userId, String userPwd) throws ServiceException {
        UserModel userModel = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(SELECT_USER_BY_ID_PWD);

            pstmt.setString(1, userId);
            pstmt.setString(2, userPwd);

            rs = pstmt.executeQuery();

            if (rs.next()) {
                userModel = new UserModel();

                userModel.setId(rs.getString("id"));
                userModel.setPassword(rs.getString("password"));
                userModel.setName(rs.getString("name"));
                userModel.setDdi(rs.getString("phone_ddi"));
                userModel.setNumber(rs.getString("phone_number"));
                userModel.setEmail(rs.getString("email"));
                userModel.setLongitude(String.valueOf(rs.getFloat("st_x")));
                userModel.setLatitude(String.valueOf(rs.getFloat("st_y")));
                userModel.setNearby(rs.getBoolean("nearby"));
                userModel.setRegion(rs.getBoolean("region"));
                userModel.setOnline(rs.getBoolean("online"));
                userModel.setImageUrl(rs.getString("image_url"));

            } else {
                // TODO : CORRECT EXCEPTION
                throw new PhoneNotFoundException();
            }
        } catch (SQLException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(rs, pstmt, con);
        }

        return userModel;
    }

    public static UserModel getUserWithID(String id) throws ServiceException {
        UserModel userModel = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(SELECT_USER_BY_ID);

            pstmt.setString(1, id);

            rs = pstmt.executeQuery();

            if (rs.next()) {
                userModel = new UserModel();

                userModel.setId(rs.getString("id"));
                userModel.setPassword(rs.getString("password"));
                userModel.setName(rs.getString("name"));
                userModel.setDdi(rs.getString("phone_ddi"));
                userModel.setNumber(rs.getString("phone_number"));
                userModel.setEmail(rs.getString("email"));
                userModel.setLongitude(String.valueOf(rs.getFloat("st_x")));
                userModel.setLatitude(String.valueOf(rs.getFloat("st_y")));
                userModel.setNearby(rs.getBoolean("nearby"));
                userModel.setRegion(rs.getBoolean("region"));
                userModel.setOnline(rs.getBoolean("online"));
                userModel.setImageUrl(rs.getString("image_url"));

            } else {
                throw new PhoneNotFoundException();
            }
        } catch (SQLException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(rs, pstmt, con);
        }

        return userModel;
    }

    public static UserModel getUserWithPhone(PhoneNumber phoneNumber) throws ServiceException {
        UserModel userModel = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(SELECT_ID_PASSWORD_FROM_USER);

            pstmt.setString(1, phoneNumber.getDdi());
            pstmt.setString(2, phoneNumber.getNumber());

            rs = pstmt.executeQuery();

            if (rs.next()) {
                String id = rs.getString(1);
                userModel = new UserModel();

                userModel.setId(id);
                userModel.setPassword(rs.getString(2));
                userModel.setDdi(phoneNumber.getDdi());
                userModel.setNumber(phoneNumber.getNumber());

            } else {
                throw new PhoneNotFoundException();
            }
        } catch (SQLException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(rs, pstmt, con);
        }

        return userModel;
    }

    // Verify Authentication Code
    public static AuthenticationEntity saveAuthentication(AuthenticationEntity authenticationEntity) throws ServiceException {
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(SAVE_AUTHENTICATION);

            pstmt.setString(1, authenticationEntity.getUserModel().getId());
            pstmt.setString(2, authenticationEntity.getCode());
            pstmt.setTimestamp(3, new Timestamp(authenticationEntity.getSendDate().getMillis()));
            pstmt.setTimestamp(4, new Timestamp(authenticationEntity.getExpirationDate().getMillis()));

            pstmt.execute();

        } catch (SQLException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(pstmt, con);
        }

        return authenticationEntity;
    }

    public static Boolean isValidCodeForUser(String code, UserModel userModel) throws ServiceException {
        Boolean isValid = false;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(VALIDATE_CODE);

            pstmt.setString(1, code);
            pstmt.setString(2, userModel.getId());

            rs = pstmt.executeQuery();

            isValid = rs.next();
        } catch (SQLException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(rs, pstmt, con);
        }

        return isValid;
    }

    // Update User Location
    public static void updateLocation(String userId, String longitude, String latitude) throws ServiceException {
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(UPDATE_USER_LOCATION);

            pstmt.setString(1, PostgisUtil.createPostgisPointByLL(longitude,latitude));
            pstmt.setString(2, userId);


            pstmt.execute();

        } catch (SQLException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(pstmt, con);
        }
    }

    // Set User Online
    public static void setUserOnline(String user, boolean status) throws ServiceException {
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(SET_USER_ONLINE);

            pstmt.setBoolean(1, status);
            pstmt.setString(2, user);

            pstmt.execute();

        } catch (SQLException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(pstmt, con);
        }
    }

    // Connect User To Room
    public static void connectToRegionChat(String userId, boolean connect) throws ServiceException {
        connectUserToRoomByQuery(userId, CONNECT_USER_REGION, connect);
    }

    public static void connectUserToNearbyChat(String userId, boolean connect) throws ServiceException {
        connectUserToRoomByQuery(userId, CONNECT_USER_NEARBY, connect);
    }

    public static void connectUserToRoomByQuery(String userId, String query, boolean connect) throws ServiceException {
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(query);

            pstmt.setBoolean(1, connect);
            pstmt.setString(2, userId);

            pstmt.execute();

        } catch (SQLException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(pstmt, con);
        }
    }

    // Get Users Based User Location
    public static List<String> getNearbyUsersFromUser(String userId) throws ServiceException {
        return getUsersIdFromQueryUser(userId, SELECT_NEARBY_IDS);
    }

    public static List<String> getNearbyOnlineUsersFromUser(String userId) throws ServiceException {
        return getUsersIdFromQueryUser(userId, SELECT_ONLINE_NEARBY_IDS);
    }

    public static List<String> getRegionUsersFromUser(String userId) throws ServiceException {
        return getUsersIdFromQueryUser(userId, SELECT_REGION_IDS);
    }

    public static List<String> getRegionOnlineUsersFromUser(String userId) throws ServiceException {
        return getUsersIdFromQueryUser(userId, SELECT_ONLINE_REGION_IDS);
    }

    public static List<String> getUsersIdFromQueryUser(String userId, String query) throws ServiceException {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<String> ids = new ArrayList<>();
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(query);

            pstmt.setString(1, userId);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                ids.add(rs.getString(1));
            }
        } catch (SQLException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(rs, pstmt, con);
        }
        return ids;
    }

    // Get Users By Lon Lat
    public static List<String> getRegionUsersFromLonLat(String lon, String lat) throws ServiceException {
        return getUsersFromLonLatByQuery(lon, lat, SELECT_REGION_IDS_BY_LON_LAT);
    }

    public static List<String> getNearbyUsersFromLonLat(String lon, String lat) throws ServiceException {
        return getUsersFromLonLatByQuery(lon, lat, SELECT_NEARBY_IDS_BY_LON_LAT);
    }

    public static List<String> getUsersFromLonLatByQuery(String lon, String lat, String query) throws ServiceException {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<String> ids = new ArrayList<>();
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(query);

            pstmt.setString(1, "SRID=4326;POINT(" + lon + " " + lat + ")");

            rs = pstmt.executeQuery();

            while (rs.next()) {
                ids.add(rs.getString(1));
            }
        } catch (SQLException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(rs, pstmt, con);
        }
        return ids;

    }


}
