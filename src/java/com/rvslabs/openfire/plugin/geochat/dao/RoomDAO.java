package com.rvslabs.openfire.plugin.geochat.dao;

import com.rvslabs.openfire.plugin.geochat.entity.Room;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;
import com.rvslabs.openfire.plugin.geochat.utils.PostgisUtil;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.jivesoftware.database.DbConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rvslabs on 17/07/15.
 */
public class RoomDAO {

    private static final Logger Log = LoggerFactory.getLogger(RoomDAO.class);


    private final static String CREATE_ROOM = "INSERT INTO room (name, description, range, location, url_image, address_description,creator) VALUES(?,?,?,ST_GeographyFromText(?),?,?,?)";
    private final static String ADD_USER_ROOM = "INSERT INTO room_member (room_id, user_id) VALUES (?,?)";
    private final static String REMOVER_USER_ROOM = "DELETE FROM room_member WHERE room_id=? AND user_id=?";

    private final static String SELECT_IN_ROOM_USER_IDS = "SELECT u.id FROM room_member ru , rvsuser u, room r WHERE (ru.room_id = ? AND u.id = ru.user_id AND r.id = ru.room_id) AND ST_DWithin(u.location, r.location, r.range)";
    private final static String SELECT_IN_ROOMS_IDS_BY_USER = "SELECT ru.room_id FROM room_member ru, room r, rvsuser u WHERE (u.id = ? AND u.id = ru.user_id AND r.id = ru.room_id) AND ST_DWithin(u.location, r.location, r.range)";
    private final static String SELECT_IN_ROOMS_IDS_BY_LON_LAT_BY_USER = "SELECT ru.room_id from room_member ru, room r, rvsuser u where (u.id = ? and u.id = ru.user_id and r.id = ru.room_id) and ST_DWithin(ST_GeographyFromText(?), r.location, r.range)";
    private final static String SELECT_ROOM_USER_IDS = "SELECT user_id FROM room_member WHERE room_id = ?";
    private final static String SELECT_ROOM_ONLINE_USER_IDS = "SELECT user_id FROM room_member, rvsuser u WHERE room_id = ? AND u.online=TRUE AND u.id = room_member.user_id";

    private final static String SELECT_ROOMS = "SELECT id, name, description, range, url_image, address_description, ST_AsGeoJSON(location::GEOMETRY) as location FROM room";

    private final static String SELECT_ROOMS_CREATED_BY_USER = "SELECT id, name, description, range, url_image, address_description, ST_AsGeoJSON(location::GEOMETRY) as location FROM room where creator = ?";
    private final static String SELECT_ROOMS_USER_PARTICIPATING = "SELECT r.id, r.name, r.description, r.range, r.url_image, r.address_description, ST_AsGeoJSON(r.location::GEOMETRY) as location from room r,room_member rm where (rm.room_id = r.id and rm.user_id = ?)";

    public static Room createRoom(Room room) throws ServiceException {
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(CREATE_ROOM, Statement.RETURN_GENERATED_KEYS);

            pstmt.setString(1, room.getName());
            pstmt.setString(2, room.getDescription());
            pstmt.setInt(3, room.getRange());
            String pointSQL = PostgisUtil.createPostgisPointByLL(room.getLongitude(), room.getLatitude());
            pstmt.setString(4, pointSQL);
            pstmt.setString(5, room.getUrlImage());
            pstmt.setString(6, room.getAddressDescription());
            pstmt.setString(7, room.getCreatorId());

            int affectedRows = pstmt.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating user failed, no rows affected.");
            }

            try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    room.setId(generatedKeys.getLong(1));
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }

            return room;

        } catch (SQLException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(pstmt, con);
        }
    }

    public static void removeRoomUser(Long roomId, String userId) throws ServiceException {

        Log.info("Removing user - " + Thread.currentThread().getStackTrace());

        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(REMOVER_USER_ROOM);

            pstmt.setLong(1, roomId);
            pstmt.setString(2, userId);

            pstmt.executeUpdate();

        } catch (SQLException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(pstmt, con);
        }
    }

    public static void addRoomUser(Long roomId, String userId) throws ServiceException {
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(ADD_USER_ROOM);

            pstmt.setLong(1, roomId);
            pstmt.setString(2, userId);

            pstmt.executeUpdate();

        } catch (SQLException sqle) {
            Log.error("Exception error: " + sqle);
//            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(pstmt, con);
        }
    }

    public static List<String> getUsersByRoom(Long roomId) throws ServiceException {
        return getUsersRoomByQuery(roomId, SELECT_ROOM_USER_IDS);
    }

    public static List<String> getUsersOnlineByRoom(Long roomId) throws ServiceException {
        return getUsersRoomByQuery(roomId, SELECT_ROOM_ONLINE_USER_IDS);
    }

    public static List<String> getUsersInRoom(long roomId) throws ServiceException {
        return getUsersRoomByQuery(roomId, SELECT_IN_ROOM_USER_IDS);
    }

    public static List<String> getUsersRoomByQuery(long roomId, String query) throws ServiceException {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<String> ids = new ArrayList<>();
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(query);

            pstmt.setLong(1, roomId);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                ids.add(rs.getString(1));
            }
        } catch (SQLException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(rs, pstmt, con);
        }
        return ids;
    }

    public static List<Long> getInRoomsUserByLonLatUser(String userId, String lon, String lat) throws ServiceException {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<Long> ids = new ArrayList<>();
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(SELECT_IN_ROOMS_IDS_BY_LON_LAT_BY_USER);

            pstmt.setString(1, userId);
            String location = PostgisUtil.createPostgisPointByLL(lon, lat);
            pstmt.setString(2, location);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                ids.add(rs.getLong(1));
            }
        } catch (SQLException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(rs, pstmt, con);
        }
        return ids;
    }

    public static List<Long> getInRoomsUserByUser(String userId) throws ServiceException {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<Long> ids = new ArrayList<>();
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(SELECT_IN_ROOMS_IDS_BY_USER);

            pstmt.setString(1, userId);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                ids.add(rs.getLong(1));
            }
        } catch (SQLException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(rs, pstmt, con);
        }
        return ids;
    }

    public static List<Room> getRooms() throws ServiceException {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<Room> rooms = new ArrayList<>();
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(SELECT_ROOMS);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                // id, name, description, range, location, url_image, address_description
                Room room = new Room();
                room.setId(rs.getLong("id"));
                room.setName(rs.getString("name"));
                room.setDescription(rs.getString("description"));
                room.setRange(rs.getInt("range"));
                room.setAddressDescription("address_description");

                JSONObject location = new JSONObject(rs.getString("location"));
                JSONArray locations = location.getJSONArray("coordinates");
                room.setLongitude(locations.getString(0));
                room.setLatitude(locations.getString(1));

                rooms.add(room);
            }
        } catch (SQLException | JSONException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(rs, pstmt, con);
        }
        return rooms;
    }

    public static List<Room> getRoomsCreatedByUser(String userId) throws ServiceException {
        return getRoomsByUserGenericQuery(SELECT_ROOMS_CREATED_BY_USER, userId);
    }

    public static List<Room> getRoomsParticipatingByUser(String userId) throws ServiceException {
        return getRoomsByUserGenericQuery(SELECT_ROOMS_USER_PARTICIPATING, userId);
    }

    public static List<Room> getRoomsSuggestionToUser(String userId) throws ServiceException {
        return getRooms();
    }

    public static List<Room> getRoomsByUserGenericQuery(String query, String userId) throws ServiceException {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<Room> rooms = new ArrayList<>();
        try {
            con = DbConnectionManager.getConnection();
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, userId);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                // id, name, description, range, location, url_image, address_description
                Room room = new Room();
                room.setId(rs.getLong("id"));
                room.setName(rs.getString("name"));
                room.setDescription(rs.getString("description"));
                room.setRange(rs.getInt("range"));
                room.setUrlImage(rs.getString("url_image"));
                room.setAddressDescription("address_description");

                JSONObject location = new JSONObject(rs.getString("location"));
                JSONArray locations = location.getJSONArray("coordinates");
                room.setLongitude(locations.getString(0));
                room.setLatitude(locations.getString(1));

                rooms.add(room);
            }
        } catch (SQLException | JSONException sqle) {
            throw new ServiceException(sqle);
        } finally {
            DbConnectionManager.closeConnection(rs, pstmt, con);
        }
        return rooms;
    }
}

