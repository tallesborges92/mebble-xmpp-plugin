package com.rvslabs.openfire.plugin.geochat;

import com.rvslabs.openfire.plugin.geochat.exceptions.RESTExceptionMapper;
import com.rvslabs.openfire.plugin.geochat.service.AuthenticationService;
import com.rvslabs.openfire.plugin.geochat.service.RoomService;
import com.rvslabs.openfire.plugin.geochat.service.UtilService;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.spi.container.servlet.ServletContainer;
import org.jivesoftware.admin.AuthCheckFilter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Class JerseyWrapper.
 */
public class JerseyWrapper extends ServletContainer {

	/** The Constant AUTHFILTER. */
	private static final String AUTHFILTER = "com.rvslabs.openfire.plugin.geochat.AuthFilter";

	/** The Constant CONTAINER_REQUEST_FILTERS. */
	private static final String CONTAINER_REQUEST_FILTERS = "com.sun.jersey.spi.container.ContainerRequestFilters";

	/** The Constant RESOURCE_CONFIG_CLASS_KEY. */
	private static final String RESOURCE_CONFIG_CLASS_KEY = "com.sun.jersey.config.property.resourceConfigClass";

	/** The Constant RESOURCE_CONFIG_CLASS. */
	private static final String RESOURCE_CONFIG_CLASS = "com.sun.jersey.api.core.PackagesResourceConfig";

	/** The Constant SCAN_PACKAGE_DEFAULT. */
	private static final String SCAN_PACKAGE_DEFAULT = JerseyWrapper.class.getPackage().getName();

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant SERVLET_URL. */
	private static final String SERVLET_URL = "geochat/*";

	/** The config. */
	private static Map<String, Object> config;

	/** The prc. */
	private static PackagesResourceConfig prc;
	
	/** The Constant JERSEY_LOGGER. */
	private final static Logger JERSEY_LOGGER = Logger.getLogger("com.sun.jersey");

	static {
		JERSEY_LOGGER.setLevel(Level.SEVERE); 
		config = new HashMap<String, Object>();
		config.put(RESOURCE_CONFIG_CLASS_KEY, RESOURCE_CONFIG_CLASS);
		prc = new PackagesResourceConfig(SCAN_PACKAGE_DEFAULT);
		prc.setPropertiesAndFeatures(config);
		prc.getProperties().put(CONTAINER_REQUEST_FILTERS, AUTHFILTER);

		prc.getClasses().add(AuthenticationService.class);
		prc.getClasses().add(UtilService.class);
        prc.getClasses().add(RoomService.class);

        prc.getClasses().add(RESTExceptionMapper.class);
	}

	/**
	 * Instantiates a new jersey wrapper.
	 */
	public JerseyWrapper() {
		super(prc);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	@Override
	public void init(ServletConfig servletConfig) throws ServletException {
		super.init(servletConfig);
		// Exclude this servlet from requering the user to login
		AuthCheckFilter.addExclude(SERVLET_URL);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sun.jersey.spi.container.servlet.ServletContainer#destroy()
	 */
	@Override
	public void destroy() {
		super.destroy();
		// Release the excluded URL
		AuthCheckFilter.removeExclude(SERVLET_URL);
	}
}
