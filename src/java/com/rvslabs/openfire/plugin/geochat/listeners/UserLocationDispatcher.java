package com.rvslabs.openfire.plugin.geochat.listeners;

import com.rvslabs.openfire.plugin.geochat.entity.UserModel;
import org.jivesoftware.openfire.event.SessionEventListener;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by rvslabs on 23/07/15.
 */
public class UserLocationDispatcher {

    private static List<UserLocationListener> listeners = new CopyOnWriteArrayList<>();


    public static void addListener(UserLocationListener listener) {
        listeners.add(listener);
    }

    public static void removeListener(UserLocationListener listener) {
        listeners.remove(listener);
    }

    public static void dispatchUserLocationChanged(UserModel user, String fromLon, String fromLat, String toLon, String toLat) {
        for (UserLocationListener listener : listeners) {
            listener.userChangeLocation(user, fromLon, fromLat, toLon, toLat);
        }
    }
}
