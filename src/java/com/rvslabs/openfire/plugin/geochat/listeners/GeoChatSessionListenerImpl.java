package com.rvslabs.openfire.plugin.geochat.listeners;

import com.rvslabs.openfire.plugin.geochat.controller.UserController;
import org.jivesoftware.openfire.event.SessionEventListener;
import org.jivesoftware.openfire.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by rvslabs on 13/07/15.
 */
public class GeoChatSessionListenerImpl implements SessionEventListener {

    private static Logger LOG = LoggerFactory.getLogger(GeoChatSessionListenerImpl.class);


    @Override
    public void sessionCreated(Session session) {
        LOG.info("User Online: " +session.getAddress().toString());
        UserController.getInstance().setUserOnline(session.getAddress().getNode(), true);
    }

    @Override
    public void sessionDestroyed(Session session) {
        LOG.info("User Offline: " +session.getAddress().toString());
        UserController.getInstance().setUserOnline(session.getAddress().getNode(), false);
    }

    @Override
    public void anonymousSessionCreated(Session session) {

    }

    @Override
    public void anonymousSessionDestroyed(Session session) {

    }

    @Override
    public void resourceBound(Session session) {

    }
}
