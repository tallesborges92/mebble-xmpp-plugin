package com.rvslabs.openfire.plugin.geochat.listeners;

import com.rvslabs.openfire.plugin.geochat.entity.UserModel;
import com.rvslabs.openfire.plugin.geochat.exceptions.ServiceException;

/**
 * Created by rvslabs on 23/07/15.
 */
public interface UserLocationListener {

    public void userChangeLocation(UserModel user, String fromLon, String fromLat, String toLon, String toLat);

}
