package com.rvslabs.openfire.plugin.geochat.entity;

import org.joda.time.DateTime;

/**
 * Created by rvslabs on 29/06/15.
 */
public class AuthenticationEntity {

    private UserModel userModel;

    private String code;

    private DateTime sendDate;

    private DateTime expirationDate;

    public AuthenticationEntity() {
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DateTime getSendDate() {
        return sendDate;
    }

    public void setSendDate(DateTime sendDate) {
        this.sendDate = sendDate;
    }

    public DateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(DateTime expirationDate) {
        this.expirationDate = expirationDate;
    }
}