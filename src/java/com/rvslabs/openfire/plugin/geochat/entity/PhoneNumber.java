package com.rvslabs.openfire.plugin.geochat.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by rvslabs on 25/06/15.
 */
@XmlRootElement(name = "phone")
public class PhoneNumber {

    private String ddi;

    private String number;

    public PhoneNumber() {
    }

    @XmlElement
    public String getDdi() {
        return ddi;
    }

    public void setDdi(String ddi) {
        this.ddi = ddi;
    }

    @XmlElement
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "PhoneNumber{" +
                "ddi='" + ddi + '\'' +
                ", number='" + number + '\'' +
                '}';
    }
}
