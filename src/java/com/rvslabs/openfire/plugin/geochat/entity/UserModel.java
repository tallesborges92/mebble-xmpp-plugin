package com.rvslabs.openfire.plugin.geochat.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class UserModel.
 */
@XmlRootElement(name = "user")
public class UserModel {

	/** The id. */
	private String id;

	/** The name. */
	private String name;

	/** The email. */
	private String email;

	/** The password. */
	private String password;

	/** The Phone ddi */
	private String ddi;

	/** The Phone Number */
	private String number;

    private boolean nearby;

    private boolean region;

    private boolean online;

    private String longitude;

    private String latitude;

	private String imageUrl;

	/**
	 * Instantiates a new user entity.
	 */
	public UserModel() {

	}

	/**
	 * Instantiates a new user entity.
	 *
	 * @param id
	 *            the id
	 * @param name
	 *            the name
	 * @param email
	 *            the email
	 */
	public UserModel(String id, String name, String email) {
		this.id = id;
		this.name = name;
		this.email = email;
	}

	@XmlElement
	public String getDdi() {
		return ddi;
	}

	public void setDdi(String ddi) {
		this.ddi = ddi;
	}

	@XmlElement
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	@XmlElement
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@XmlElement
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	@XmlElement
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email
	 *            the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
    @XmlElement
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password
	 *            the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}


    public String getInternationalNumber() {
        return this.getDdi() + this.getNumber();
    }


    public boolean isNearby() {
        return nearby;
    }

    public void setNearby(boolean nearby) {
        this.nearby = nearby;
    }

    public boolean isRegion() {
        return region;
    }

    public void setRegion(boolean region) {
        this.region = region;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }


	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
}
