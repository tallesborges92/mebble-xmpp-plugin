package com.rvslabs.openfire.plugin.geochat.utils;

/**
 * Created by rvslabs on 17/07/15.
 */
public class PostgisUtil {


    public static String createPostgisPointByLL(String longitude, String latitude) {
        return "SRID=4326;POINT(" + longitude + " " + latitude + ")";
    }
}
