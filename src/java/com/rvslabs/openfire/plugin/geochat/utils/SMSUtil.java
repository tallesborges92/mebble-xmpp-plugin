package com.rvslabs.openfire.plugin.geochat.utils;

import com.rvslabs.openfire.plugin.geochat.entity.AuthenticationEntity;
import oneapi.client.impl.SMSClient;
import oneapi.config.Configuration;
import oneapi.model.SMSRequest;

/**
 * Created by rvslabs on 29/06/15.
 */
public class SMSUtil {


    public static boolean sendAuthenticationSMS(AuthenticationEntity authenticationEntity) {
        Configuration configuration = new Configuration("rvslabs", "Rvslabs2015");

        SMSClient smsClient = new SMSClient(configuration);

        SMSRequest smsRequest = new SMSRequest("RVS Labs", authenticationEntity.getCode(), authenticationEntity.getUserModel().getInternationalNumber());

        smsClient.getSMSMessagingClient().sendSMS(smsRequest);

        return false;
    }


}
