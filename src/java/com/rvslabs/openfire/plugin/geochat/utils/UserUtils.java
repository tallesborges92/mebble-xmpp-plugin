package com.rvslabs.openfire.plugin.geochat.utils;

import java.util.UUID;

/**
 * The Class UserUtils.
 */
public class UserUtils {

	/**
	 * Instantiates a new user utils.
	 */
	private UserUtils() {
		throw new AssertionError();
	}

    /**
     * @return
     */
	public static String UUIDWithoutDashes() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}


}
