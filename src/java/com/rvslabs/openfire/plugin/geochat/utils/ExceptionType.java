package com.rvslabs.openfire.plugin.geochat.utils;

/**
 * The Class ExceptionType.
 */
public final class ExceptionType {

	public static final String ILLEGAL_ARGUMENT_EXCEPTION = "IllegalArgumentException";
	
	public static final String NOT_ALLOWED = "NotAllowedException";

	public static final String CODE_INVALID_MESSAGE = "The code is invalid.";
    public static final int CODE_INVALID_CODE = 1;

	public static final String PHONE_INVALID_MESSAGE = "The phone is invalid.";
    public static final int PHONE_INVALID_CODE = 2;

    public static final String NUMBER_NOT_FOUND_MESSAGE = "The number is invalid";
    public static final int NUMBER_NOT_FOUND_CODE = 3;


	private ExceptionType() {
	}
}
